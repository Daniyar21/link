import React, {useState} from 'react';
import './ShortLink.css';
import {useDispatch} from "react-redux";
import {createLink} from "../../store/actions/linksAction";

const ShortLink = ({onSubmit}) => {

    const [userInput, setUserInput] = useState({
        originalUrl: ''
    })

    const dispatch = useDispatch();

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setUserInput(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const onClick = e =>{
        e.preventDefault();
       onSubmit(userInput);
        setUserInput({originalUrl: ""})
    }

    return (
        <div className='link-block'>
            <h2>Shorten your link</h2>
            <input
                type="text"
                name="originalUrl"
                value={userInput.originalUrl}
                onChange={inputChangeHandler}
            />
            <button onClick={onClick} >Shorten</button>
        </div>
    );
};

export default ShortLink;