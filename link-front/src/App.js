import './App.css';
import ShortLink from "./components/ShortLink/ShortLink";
import {useDispatch, useSelector} from "react-redux";
import {createLink} from "./store/actions/linksAction";

const App = () => {
    const dispatch =useDispatch();
    const linkObj = useSelector(state => state.links.linkObject);

    const onSubmit = async (data)=>{
        await dispatch(createLink(data));
    }

    console.log(linkObj)
  return (
      <div className="App">
          <ShortLink onSubmit={onSubmit}/>
          {linkObj && (
              <div>
                  Your link looks like this now
                  <a href={`http://localhost:8000/${linkObj.shortUrl}`}>http://localhost:8000/{linkObj.shortUrl}</a>
              </div>
          )}
      </div>
  );
};

export default App;
