import {CREATE_LINK_FAILURE, CREATE_LINK_REQUEST, CREATE_LINK_SUCCESS} from "../actions/linksAction";


const initialState = {
  fetchLoading: false,
  linkObject: null,
};

const linksReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_LINK_REQUEST:
      return {...state, fetchLoading: true};
    case CREATE_LINK_SUCCESS:
      return {...state,  fetchLoading: false, linkObject: action.payload};
    case CREATE_LINK_FAILURE:
      return {...state, fetchLoading: false};
    default:
      return state;
  }
};

export default linksReducer;