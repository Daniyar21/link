const express = require('express');
const cors = require('cors');
const links = require('./app/links');
const mongoose = require('mongoose');
const Link = require("./models/Link");

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/links', links);

app.get('/:url', async (req, res) => {
  try {
    const linkObject = await Link.findById(req.params.url);

    if (linkObject) {
      res.status(301).redirect(linkObject.originalUrl);
    } else {
      res.sendStatus(404).send({error: 'not valid'});
    }
  } catch {
    res.sendStatus(500);
  }

});


const run = async () => {
    await mongoose.connect('mongodb://localhost/links');

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });


    // process.on('exit', async () => {
    //     console.log('exiting');
    //     await mongoDb.disconnect();
    // });
};

run().catch(e => console.error(e));